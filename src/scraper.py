import smtplib

import requests
import time
from bs4 import BeautifulSoup

# simple scraper for tracking prices from hinta.fi
# sending mail hasn't been implemented completely yet

# the product you wish to track
URL = 'https://hinta.fi/1525083/lg-32gk650f-b'

# google "my user agent" and replace with your own
headers = {"User-Agent": 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0'}


def check_price():
    page = requests.get(URL, headers=headers)

    # parses the page to html
    soup = BeautifulSoup(page.content, 'html.parser')

    # print(soup.prettify())

    print(soup.h1.text)

    # scrape the prices from html, convert to text, replace commas with dots that can be converted to float later on
    # use the browser inspect element feature on the item you want to scrape
    price = soup.find('p', attrs={"class": "hv--price1"}).text.replace(',', '.')
    price2 = soup.find('p', attrs={"class": "hv--price2"}).text.replace(',', '.')

    print(price)
    print(price2)

    # take the first 6 characters
    short_price = float(price[0:6])
    short_price2 = float(price2[0:6])

    # if price drops under 400.00€
    if short_price < 400.00 or short_price2 < 400.00:
        print('mail sent')
        # send_mail()


# enable less secure apps from google, generate app password
def send_mail():
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()  # command for identifying when connecting to an another email server
    server.starttls()  # encrypts the connection
    server.ehlo()

    server.login('user@gmail.com', 'generated_password_here')

    subject = 'The product is on sale!'
    body = 'https://hinta.fi/1525083/lg-32gk650f-b on nyt tarjouksessa'

    msg = f"Subject: {subject}\n\n{body}"

    # from, to, message
    server.sendmail(
        'user@gmail.com',
        'anotherUser@gmail.com',
        msg
    )
    # close the connection
    server.quit


while (True):
    check_price()
    time.sleep(86400)  # 24h wait before running again
